package main

import (
	"bytes"
	"crypto/sha1"
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/tv42/zbase32"
	"golang.org/x/crypto/acme/autocert"
	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
)

var cache = map[string][]byte{}
func updateKeys(dirStr string) {
	dir, err := os.ReadDir(dirStr)
	if err != nil {
		log.Fatal(err)
	}
	if dirStr == "." {
		dirStr = "*"
	}
	for _, domainFile := range dir {
		if domainFile.IsDir() && dirStr == "*" {
			updateKeys(domainFile.Name())
		} else if !domainFile.IsDir() {
			trimmedName := strings.TrimSuffix(strings.TrimSuffix(domainFile.Name(), ".pem"), ".asc")
			hashedLocalPart := hash(trimmedName)

			dirPrefix := dirStr + string(os.PathSeparator)
			if dirStr == "*" {
				dirPrefix = ""
			}
			contents, err := os.ReadFile(dirPrefix + domainFile.Name())
			if err != nil {
				log.Fatal(err)
			}
			
			block, err := armor.Decode(bytes.NewReader(contents))
			if err != nil && !isPGPKey(contents) {
				if old, ok := cache[hashedLocalPart + "@" + dirStr]; !ok || old != nil {
					log.Printf("Skipping invalid key: %s@%s", trimmedName, dirStr)
					cache[hashedLocalPart + "@" + dirStr] = nil
				}
			} else if err != nil {
				if old, ok := cache[hashedLocalPart + "@" + dirStr]; !ok || !bytes.Equal(old, contents) {
					log.Printf("Loaded binary key: %s@%s", trimmedName, dirStr)
					cache[hashedLocalPart + "@" + dirStr] = contents
				}
			} else {
				blockContents, err := io.ReadAll(block.Body)
				if err != nil {
					log.Fatal(err)
				}
				if old, ok := cache[hashedLocalPart + "@*"]; !ok || !bytes.Equal(old, blockContents) {
					if !isPGPKey(blockContents) {
						log.Printf("Skipping invalid key: %s@%s", trimmedName, dirStr)
						cache[hashedLocalPart + "@*"] = nil
					} else {
						log.Printf("Loaded PEM key: %s@%s", trimmedName, dirStr)
						cache[hashedLocalPart + "@*"] = blockContents
					}
				}
			}
		}
	}
}

func isPGPKey(block []byte) bool {
	el, err := openpgp.ReadKeyRing(bytes.NewReader(block))
	if err != nil || len(el) < 1 {
		return false
	}
	for _, e := range el {
		if e.PrivateKey != nil {
			return false
		}
	}
	return true
}

func main() {
	app := mux.NewRouter()
	
    app.HandleFunc("/policy", policy)
    app.HandleFunc("/{domain}/policy", policy)
    app.HandleFunc("/.well-known/openpgpkey/{domain}/policy", policy)
	app.HandleFunc("/.well-known/openpgpkey/policy", policy)
	
    app.HandleFunc("/hu/{hash}", pubkey)
	app.HandleFunc("/{domain}/hu/{hash}", pubkey)
	app.HandleFunc("/.well-known/openpgpkey/hu/{hash}", pubkey)
	app.HandleFunc("/.well-known/openpgpkey/{domain}/hu/{hash}", pubkey)
	
	go func() {
		for {
			updateKeys(".")
			time.Sleep(5 * time.Second)
		}
	}()
	
	log.Printf("Starting webserver...")
	if os.Getenv("LE_DOMAIN") != "" {
		if os.Getenv("LE_ACCEPT_TERMS") != "yes" {
			log.Fatal(errors.New("You must accept the Let's Encrypt terms of service by setting LE_ACCEPT_TERMS to \"yes\"."))
		} else {
			log.Fatal(http.Serve(autocert.NewListener(strings.Split(os.Getenv("LE_DOMAIN"), " ")...), app))
    	}
    } else {
    	if port := os.Getenv("PORT"); port != "" {
    		log.Fatal(http.ListenAndServe(":" + port, app))
    		return
    	}
    	log.Fatal(http.ListenAndServe(":80", app))
    }
}

func policy(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Server", "wkdd")
	w.Header().Set("Content-Type", "text/plain")
	w.Header().Set("Content-Length", "0")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte{})
}

func pubkey(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	if _, ok := vars["domain"]; !ok {
		vars["domain"] = r.Header.Get("Host")
	}
	content, _ := cache[vars["hash"] + "@" + vars["domain"]]
	if content == nil {
		content, _ = cache[vars["hash"] + "@*"]
	}
	if content == nil {
		http.NotFoundHandler().ServeHTTP(w, r)
		return
	}

	w.Header().Set("Server", "wkdd")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Length", strconv.Itoa(len(content)))
	w.WriteHeader(http.StatusOK)
	w.Write(content)
}

func hash(localPart string) string {
	hash := sha1.New()
	io.WriteString(hash, localPart)
	hashBytes := hash.Sum(nil)
	return zbase32.EncodeToString(hashBytes)
}
