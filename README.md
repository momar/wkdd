# WKD Daemon

Daemon for serving PGP keys via [Web Key Directory](https://wiki.gnupg.org/WKD).

It responds to routes on `[/.well-known/openpgpkey][/DOMAIN.TLD]/hu/*`, `[/.well-known/openpgpkey][/DOMAIN.TLD]/policy`, with each bracketed part being optional.

Usage: `docker run -d -v "/etc/wkdd-keys:/keys" --restart always momar/wkdd`

The structure of the `/keys` folder is as follows:
- single-domain: `example.pem` - key for `example@*`
- multi-domain: `example.org/hello.pem` - key for `hello@example.org`

Additional environment variables for Let's Encrypt (when not using a reverse proxy):
- `LE_DOMAINS=example.org openpgpkey.example.org` - domains to get certificates for, usually your email domain and/or the `openpgpkey` subdomain.
- `LE_ACCEPT_TERMS=yes` - accept Let's Encrypt's [Terms of Service](); required for `LE_DOMAINS`.

Note that when not serving WKD from the `openpgpkey` subdomain, you will have to either ensure that said subdomain doesn't resolve to anything (note that this includes wildcards), or that it serves a TXT record with any content (for example the string `"empty"`, as actually empty TXT records are not possible).
