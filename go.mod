module codeberg.org/momar/wkdd

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/tv42/zbase32 v0.0.0-20190604154422-aacc64a8f915
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
