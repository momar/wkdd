FROM golang:alpine
RUN apk add --no-cache git upx
COPY go.mod go.sum /build/wkdd/
WORKDIR /build/wkdd
RUN go mod download
COPY *.go /build/wkdd/
RUN CGO_ENABLED=0 go build -a -ldflags '-s -w -extldflags "-static"' -o /build/wkdd.bin .
RUN upx /build/wkdd.bin

FROM scratch
COPY --from=0 /build/wkdd.bin /bin/wkdd
WORKDIR /keys
EXPOSE 80
ENTRYPOINT ["/bin/wkdd"]
